class Empleado:
    # transformamos funciones en métodos
    # métodos funciones dentro de la clase (tienen lo de self)
    # al estar dentro de la clase no hay que volver a meter los datos, porqwue como ya están dentro de la clase tiene esos datos
    
    def __init__(self, n, s):
        # los nombre, nomina... no son variables, son atributos/miembros
        self.nombre = n
        self.nomina = s
    
    def calculo_impuestos(self):
        impuestos = self.nomina*0.30
        return impuestos
    
    #cambiamos el metodo por un metodo especial (los de ls __algo__)
    #def imprime(self):
        #tmp = self.calculo_impuestos()
        #print ("El empleado {name} debe pagar {tax:.2f}".format(name=self.nombre,tax=tmp))

    def __str__(self):
        return "El empleado {name} debe pagar {tax:.2f}".format(name=self.nombre,tax=self.calculo_impuestos())



empleadoPepe = Empleado( "Pepe", 20000)
empleadaAna = Empleado( "Ana",30000)

total = empleadoPepe.calculo_impuestos() + empleadaAna.calculo_impuestos()
# el .calculo_impuestos ahora no es para que imprima eso es para que haga el métdo ese y saque el resultado
# instancia=objeto
# metodo (orientacion objetos): instancia quiero que hagas esto
# self es como un comodín: pepe, ana...

#tmb cambiamos esto
#empleadoPepe.imprime()
#empleadaAna.imprime()

print(empleadoPepe)
print(empleadaAna)
print("Los impuestos a pagar en total son {:.2f} euros".format(total))
 # .2f  son 2 decimales

#metodos: funciones especiales para los empleados

