import unittest
from mul import multiply
from mul import add

class MultiplyTestCase(unittest.TestCase):
    #con este solo sale 1 test y ok, porque 5*5=25
    def test_multiplication_with_correct_values(self):
        self.assertEqual(multiply(5, 5), 25)
    #si añades este salen 2 test failed, porque 5*5!=24
    def test_multiplication_with_incorrect_values(self):
        self.assertEqual(multiply(5,5), 24)
    # este sale que está bien (hay que llamar al metodo de 2 formas diferentes)
    def test_multiplication_with_incorrect_values2(self):
        self.assertNotEqual(multiply(5,5), 24)
    def test_add_with_correct_values(self):
        self.assertEqual(add(5,5), 10)

if __name__ == '__main__':
    unittest.main()