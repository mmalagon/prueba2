import ejercicio_clases
# si hubiese en ese programa otra clase: Cuadrado
# from ejercicio_clases import Rectangulo
# si queremos usar las 2, usamos ejercicio_clases.Cuadrado o ejercicio_clases.Rectangulo
import unittest

class RectanguloTestCase(unittest.TestCase):
    # el metodo que queramos testear tiene que empezar por test
    def test_perimetro(self):
        rectangulo1 = Rectangulo(1,2)
        perimetro1 = rectangulo1.calculo_perimetro(2,3)
        self.assertNotEqual(perimetro1, 25)
    
    #def test_area(self):

unittest.main()