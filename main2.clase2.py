class Empleado:
    # todos los empleados (clase) tienen un nombre, nomina...(apartados)
    # def init es el cosntructor (porq es el que monta la estructura de datos)
    # self es para ver a quien apunta (por ej sustituimos self por empleadoPepe o empleadaAna )
    # todo lo que sea __algo__ es de python, sino es tuyo
    # usa: python tutor para ver lo que va haciendo
    # 1 clase: empleado
    # 2 instancias: empleadoPepe y empleadoAna
    # objeto: .....
    def __init__(self, n, s):
        self.nombre = n
        self.nomina = s

empleadoPepe = Empleado("Pepe", 20000)
empleadoAna = Empleado("Ana", 30000)

print("El nombre del empleado es {} y su salario es {}".format(empleadoPepe.nombre, empleadoPepe.nomina))
# mete lo del format(empleadoPepe.nombre) en los huecos{}
# el empleadoPepe es la tablita y para ver que imprimir
# lo del .nombre es dame el nombre de empleadoPepe, también nómina...
# el self significa lo de empleadoPepe...
