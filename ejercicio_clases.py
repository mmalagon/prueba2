class Rectangulo:
    """comentario: documentación de la clase"""
    def __init__(self, b=1, h=3):
        # al poner l=1 por defecto cogería el 1, sino el que le metas
        self.base = b
        self.altura = h
    
    ## sería en todo calcula no calculo
    def calculo_perimetro (self):
        return self.base*2 + self.altura*2
    
    def calculo_area (self):
        return self.base*self.altura
    
    def __str__(self):
        return "El área del rectangulo es {area} y el perímetro es {perimetro}.".format(area=self.calculo_area(),perimetro=self.calculo_perimetro())

#estos son los objetos
rectangulo1 = Rectangulo(5, 4) # b=5, h=4
rectangulo2 = Rectangulo() # b=1, h=3

print(rectangulo1)
print(rectangulo2)
print(rectangulo1.base)
# tmb se puede poner así: print(rectangulo1.calcula_perimetro())...
# MAL: print(perimetro(rectangulo1)): para llamar funciones

# las variables nacen y mueren en el mismo sitio (por ejemplo del constructor)